#!/bin/bash

landing="https://www.ecdc.europa.eu/en/publications-data/download-todays-data-geographic-distribution-covid-19-cases-worldwide"

v0='XLSX-316.76 KB' 
while true; do 
	v1="`curl -s $landing | grep -om1 'XLSX-[0-9\.]\+..B'`"
	echo Found $v1
	if [ "$v1" != "$v0" ]; then
	        echo $v1 is new
		git checkout .
		git pull
		python3 ./fit2.py
		git diff --exit-code output.xls > /dev/null
		if [ $? -ne 0 ]; then
			echo New results
			git add index.html output.xls output_deaths.xls
			git commit -m '`date +%F` $v1'
			git push
			v0="$v1"
			echo Sleeping until 10am
			sleep $(( $(date -d '1000 tomorrow' +%s) - $(date +%s) ))
		else
			rm COVID-19-geographic-disbtribution-worldwide*.xlsx
		fi
	fi
	echo Sleeping for 5m
	sleep 5m
done
