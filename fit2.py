#!/usr/bin/env python3

# Authors: Sean J. Delaney Ph.D., Prof. Turlough P. Downes Ph.D. (DCU)
# Copyright: See README.txt, enclosed herewith
# email: seanogdelaney@gmail.com
# Python script to analyse ECDC Covid-19 Geographic Distribution Spreadsheet
# Arguments: None
# Output: 
# 1. output.xls : A spreadsheet for the EU, tabulated by date \ country
#   Sheet 1: Summary
#   Sheet 2: Table of incidence
#   Sheet 3: Table of prevalence
#   Sheet 4: Estimated instantaneous exponent of growth in prevalence (5 Day)
#   Sheet 5: Estimated instantaneous doubling time based on sheet 3
# 2. Plots:
#   Subplot 1: Exponential fit curves with data points
#   Subplot 2: Instantaneous doubling time estimates (vs day since "day 0")

import numpy as np                  # for numerical analysis
from scipy import stats             # for kurtosis etc
from scipy.optimize import curve_fit # for curve fitting
import statsmodels.api as sm        # For linear regression

import xlrd, xlwt                   # for xls file IO
import re                           # to find xls link in landing page
import requests                     # for web access
import os.path                      # for filesystem access

from datetime import datetime, timedelta # For readable dates
import itertools                    # For line colour cycling

# Various pieces of the bokeh interactive plotting package
from bokeh.plotting import figure as bkfig, output_file, show
from bokeh.layouts import row, column
from bokeh.models import DatetimeTickFormatter, Range1d, Div, Label, Paragraph
from bokeh.palettes import Category10 as cList

# Fetch the ECDC page where the latest file is published
page = requests.get('https://www.ecdc.europa.eu/en/publications-data/download-todays-data-geographic-distribution-covid-19-cases-worldwide').text
# Find the url of the latest published file, and download it
url = re.findall('https://www.ecdc.europa.eu/sites/default/files/documents/COVID-19-geographic-disbtribution-worldwide.*xlsx', page)[0]
fname = os.path.basename(url)       # Extract file name
if not os.path.isfile(fname):       # If not present, download file
    r = requests.get(url, allow_redirects=True)
    open(fname,'wb').write(r.content)
if os.path.isfile(fname):           # If present, print identified file name
    print('Found', fname)
else:                               # otherwise, quit with message
    print("Couldn't find latest COVID-19-geographic-disbtribution-worldwide")
    quit()

# Open xls workbook, first sheet
wb = xlrd.open_workbook(fname)
sheet = wb.sheet_by_index(0)

# Extract 3 columns: dates, cases, and geoid (country code)
dates = sheet.col_values(0) # stored as days since 30/12/1899 apparently
cases = sheet.col_values(4)
deaths = sheet.col_values(5)
geoid = sheet.col_values(7)

# Export incidence sheet, prevalence sheet, exponents + error + doubling time sheet
# Create a new xls workbook
workbook_cases = xlwt.Workbook()
workbook_deaths = xlwt.Workbook()
minDate = int(min(dates[1:]))
maxDate = int(max(dates[1:]))
ord0 = datetime(1899, 12, 30).toordinal()
print(datetime.fromordinal(ord0 + maxDate).strftime("%Y-%m-%d"))
date_xf = xlwt.easyxf(num_format_str='DD/MM/YYYY')

print("")
print("")
print("############################################")
print("#                                          #")
print("#        Confirmed Case Analysis           #")
print("#                                          #")
print("############################################")
print("")
print("")

# Set up cases workbook, then we'll do deaths workbook

# Add a sheet for summary
sumSheet = workbook_cases.add_sheet('Summary')
sumSheet.write(0, 0, "Country")
sumSheet.write(0, 1, "Incidence")
sumSheet.write(0, 2, "Cumulative")
sumSheet.write(0, 3, "Doubling time (days, 3-day fit)")
sumSheet.write(0, 4, "Doubling time (days, 5-day fit)")
# Add a sheet for incidence
incSheet = workbook_cases.add_sheet('Incidence')
incSheet.write(0, 0, "Date")
for date in range(maxDate, minDate - 1, -1):
    incSheet.write(maxDate - date + 1, 0, date, date_xf)
# Add a sheet for prevalence
preSheet = workbook_cases.add_sheet('Cumulative')
preSheet.write(0, 0, "Date")
for date in range(maxDate, minDate - 1, -1):
    preSheet.write(maxDate - date + 1, 0, date, date_xf)
# Add a sheet for 3-Day Exponent of Growth in Prevalence
co3Sheet = workbook_cases.add_sheet('Cumulative Exponent (3 Day)')
co3Sheet.write(0, 0, "Date (retrospective fit)")
co3Sheet.write(0, 1, "Date (centred fit)")
for date in range(maxDate, minDate - 1, -1):
    co3Sheet.write(maxDate - date + 1, 0, date, date_xf)
    co3Sheet.write(maxDate - date + 2, 1, date, date_xf)
# Add a sheet for 3-Day Estimate of Doubing time
dbl3Sheet = workbook_cases.add_sheet('Doubling time (3 Day)')
dbl3Sheet.write(0, 0, "Date (retrospective fit)")
dbl3Sheet.write(0, 1, "Date (centred fit)")
for date in range(maxDate, minDate - 1, -1):
    dbl3Sheet.write(maxDate - date + 1, 0, date, date_xf)
    dbl3Sheet.write(maxDate - date + 2, 1, date, date_xf)
# Add a sheet for 5-Day Exponent of Growth in Prevalence
co5Sheet = workbook_cases.add_sheet('Cumulative Exponent (5 Day)')
co5Sheet.write(0, 0, "Date (retrospective fit)")
co5Sheet.write(0, 1, "Date (centred fit)")
for date in range(maxDate, minDate - 1, -1):
    co5Sheet.write(maxDate - date + 1, 0, date, date_xf)
    co5Sheet.write(maxDate - date + 3, 1, date, date_xf)
# Add a sheet for 5-Day Estimate of Doubing time
dbl5Sheet = workbook_cases.add_sheet('Doubling time (5 Day)')
dbl5Sheet.write(0, 0, "Date (retrospective fit)")
dbl5Sheet.write(0, 1, "Date (centred fit)")
for date in range(maxDate, minDate - 1, -1):
    dbl5Sheet.write(maxDate - date + 1, 0, date, date_xf)
    dbl5Sheet.write(maxDate - date + 3, 1, date, date_xf)


# List the subset of country codes, nominally:
country = ["Austria", "Belgium", "Bulgaria", "Croatia", "Cyprus", "Czech Republic", "Denmark", "Estonia", "Finland", "France", "Germany", "Greece", "Hungary", "Republic of Ireland", "Italy", "Latvia", "Lithuania", "Luxembourg", "Malta", "Netherlands", "Poland", "Portugal", "Romania", "Slovakia", "Slovenia", "Spain", "Sweden", "United Kindom of Great Britain and Northern Ireland"]

euGeoids = ["IE", "AT", "BE", "BG", "HR", "CY", "CZ", "DK", "EE", "FI", "FR", "DE", "EL", "HU", "IT", "LV", "LT", "LU", "MT", "NL", "PL", "PT", "RO", "SK", "SL", "ES", "SE", "UK", "US"]

# A shorter list for less cluttered plots
#euGeoids = ["IE", "ES", "FR", "DE", "IT", "UK"]

# Some initialisation for plots
style = ['-', '--', '-.', ':']
expn = 0
s1 = bkfig(width=600, height=700, x_axis_type='datetime')
s2 = bkfig(width=600, height=700, x_axis_type='datetime')

# Deaths
s3 = bkfig(width=600, height=700, x_axis_type='datetime')
s4 = bkfig(width=600, height=700, x_axis_type='datetime')


idx = 0
# For each chosen country
for g, c in zip(euGeoids, itertools.cycle(cList[10])):
    idx = idx + 1
    # Find the relevant rows, and extract days and case counts
    rows = [i for i in range(len(geoid)) if geoid[i] == g]
    day = np.flip( np.array( [dates[i] for i in rows] ).astype(float) )
    inc = np.flip( np.array( [cases[i] for i in rows] ).astype(float) )

    # Write summary data to sheet
    sumSheet.write(idx, 0, g)
    if len(inc) > 0:
        sumSheet.write(idx, 1, int(inc[-1]))
    sumSheet.write(idx, 2, int(np.sum(inc)))

    # Write incidence and prevalence values to their sheets
    incSheet.write(0, idx, g) # Header in row 0
    for d, val in zip(day, inc):
        incSheet.write(int(maxDate - d) + 1, idx, val)
    preSheet.write(0, idx, g) # Header in row 0
    for d, val in zip(day, np.cumsum(inc)):
        preSheet.write(int(maxDate - d) + 1, idx, val)

    # Remove days with no detections
    day = day[inc != 0]
    inc = inc[inc != 0]
    
    # Find prevalence
    pre = np.cumsum(inc)
    incDays = day           # day will be modified with pre
    
    # Spreadsheet of Exponents
    # Write the header in row 0
    co3Sheet.write(0, idx+1, g)
    dbl3Sheet.write(0, idx+1, g)
    co5Sheet.write(0, idx+1, g)
    dbl5Sheet.write(0, idx+1, g)
    if day.size < 3:
        print("Error: Insufficient prevalence " + g)
        continue
    # Prep some variables and loop from day[0] to last day
    day0 = int(day[0])
    dbl = np.zeros(maxDate - day0 + 1)
    ci = np.zeros((2, maxDate - day0 + 1))
    for i in range(day0, maxDate + 1):
        # Find the index of the data closest to target date
        ii = (np.abs(day - i)).argmin()
        # Perform the local fit, store exponent and doubling time in sheets
        if ii > 1 and ii < len(day):
            co3 = np.polyfit(day[ii-2:ii+1], np.log(pre[ii-2:ii+1]), 1)[0]
            co3Sheet.write(int(maxDate - i) + 1, idx+1, round(co3, 2))
            dbl3Sheet.write(int(maxDate - i) + 1, idx+1, round(np.log(2) / co3, 2))
        if ii > 3 and ii < len(day):
            co5 = np.polyfit(day[ii-4:ii+1], np.log(pre[ii-4:ii+1]), 1)[0]
            f = sm.OLS(np.log(pre[ii-4:ii+1]), sm.add_constant(day[ii-4:ii+1])).fit()
            co5 = f.params[1]       # slope
            ci5 = f.conf_int(0.05)[1] # slope confidence 
            co5Sheet.write(int(maxDate - i) + 1, idx+1, round(co5, 2))
            dbl5Sheet.write(int(maxDate - i) + 1, idx+1, round(np.log(2) / co5, 2))
            dbl[i - day0] = np.log(2) / co5
            ci[:, i - day0] = np.log(2) / ci5
    sumSheet.write(idx, 3, round(np.log(2)/co3, 2))
    sumSheet.write(idx, 4, round(np.log(2)/co5, 2))

    # Only fit curve when prevalence exceeds 20
    age = day[-1] - day0
    day = day[pre > 10]
    if day.size < 3:
        print("Error: Insufficient prevalence " + g)
        continue
    pre = pre[pre > 10]
    dbl = dbl[int(day[0]) - day0:] # Cut off noisy doubling data (low prevalence)
    ci = ci[:, int(day[0]) - day0:]

    # Fit exponent only by normalising on last day
    preMag = pre[-1]
    pre = pre / preMag      # pre is now fractional, ending in 1
    dateShift = day[-1]
    day = day - dateShift   # day is now negative (integers), ending in 0
    
    # Linear fit of log data
    coef1 = np.polyfit(day, np.log(pre), 1)
    #coef1 = np.linalg.lstsq(day[:, None], np.log(pre[:, None]), rcond=None)
    #print(coef1)
   
    # Weighted linear fit of log data
    coef2 = np.polyfit(day, np.log(pre), 1, w=np.sqrt(pre))
    #print(coef2)

    # Fit exponential directly via arbitrary function fit
    #f = lambda t, a: np.exp(a * t) # Function to fit
    #f = lambda t, c, a: c * np.exp(a * t) # Function to fit
    f = lambda t, *p: p[1] * np.exp(p[0] * t)
    coef3 = curve_fit(f,  day,  pre, p0=[1, 0.25]) #, sigma=pre)
    coef3 = coef3[0]
    print(g, coef3[0], coef2[0], coef1[0])

    # Fit a tanh function.  Not interfering with previous work 
    # as fitting the exponential gives info on the doubling time
    f1 = lambda t, *tanh_p: tanh_p[0]*(1.0+np.tanh((t-tanh_p[2])/tanh_p[1]))
    try:
        tanh_coef = curve_fit(f1, day, pre, p0=[500.0, 30.0, 0.0])
    except:
        print("Failed to fit tanh for",g)
    else:
        tanh_coef = tanh_coef[0]
        print("Tanh fit:",tanh_coef[0], tanh_coef[1], tanh_coef[2])
    tanh0 = tanh_coef[0]
    tanh1 = tanh_coef[1]
    tanh2 = tanh_coef[2]

    # National age of epidemic inferred by exponential from 1 case
    scal = coef3[1]
    expn = coef3[0]
#    if expn > 0:
#        age = np.log(preMag) / expn # y = 1/preMag = exp(a*t) => -log(preMag) = a*t
#    else:
#        age = -day[0]
    
    # Label string for plotting 
    lab = g + " %.2f" % expn
    if expn <= 0:
        print(lab)

    # bokeh plot
    r = set()
    # Original data darkers
    x0 = incDays.astype(int)   # Recover excel dates
    x0 = [datetime.fromordinal(datetime(1900, 1, 1).toordinal() + xi - 2) for xi in x0] # Excel date to python datetime
    fArr1 = [s1.circle, s1.square, s1.triangle]
    r.add(fArr1[idx//10](x0, np.cumsum(inc), color=c, alpha=0.8, legend_label=lab))
    # Fit line
    x1 = (day + day0 + age).astype(int)   # Recover excel dates
    x1 = [datetime.fromordinal(datetime(1900, 1, 1).toordinal() + xi - 2) for xi in x1] # Excel date to python datetime
    r.add(s1.line(x1, preMag * scal * np.exp(day * expn), color=c, alpha=0.8, legend_label=lab))
    # Add tanh fit (I think!)
    r.add(s1.line(x1, preMag*tanh0*(1.0+np.tanh((day-tanh2)/tanh1)), color=c, alpha=0.8, legend_label=lab))
    # Doubling line
    x2 = (np.arange(0, len(dbl)) + 1 - len(dbl) + day0 + age).astype(int)   # Recover excel dates
    x2 = [datetime.fromordinal(datetime(1900, 1, 1).toordinal() + xi - 2) for xi in x2] # Excel date to python datetime
    dash = ['solid', 'dashed', 'dotted', 'dotdash']
    r.add(s2.line(x2, dbl, color=c, legend_label=g, line_dash=dash[idx//10], line_width=2))
    # Doubling markers
    fArr2 = [s2.circle, s2.square, s2.triangle]
    r.add(fArr2[idx//10](x2, dbl, color=c, legend_label=g))
    errX = []
    errY = []
    for x, c0, c1 in zip(x2, ci[0,:], ci[1,:]):
        errX.append((x, x))
        errY.append((c0, c1))
    r.add(s2.multi_line(errX, errY, color=c, legend_label=g))
    # Fit Irish extrapolated markers
    expn = co3
    extX = [datetime(2020, 3, 31), datetime(2020, 4, 20)]
    if idx == 1:
        sumSheet.write(0, 5, extX[0].strftime("%d/%m/%Y"))
        sumSheet.write(0, 6, extX[1].strftime("%d/%m/%Y"))
    extY = [preMag * scal * np.exp((dt.toordinal() - ord0 - day0 - age) * expn) for dt in extX]
    sumSheet.write(idx, 5, int(extY[0]))
    sumSheet.write(idx, 6, int(extY[1]))
    r.add(fArr1[idx//10](extX, extY, color=c, fill_color="white", legend_label=lab))
    extXfull = [x1[-1] + timedelta(days=d) for d in range(21)]
    extYfull = [preMag * scal * np.exp((dt.toordinal() - ord0 - day0 - age) * expn) for dt in extXfull]
    r.add(s1.line(extXfull, extYfull, color=c, line_dash="dashed", alpha=0.8, legend_label=lab))
    # Add tanh fit (I think!)
    extYfull = [preMag*tanh0*(1.0+np.tanh((dt.toordinal() - ord0 - day0 - age-tanh2) / tanh1)) for dt in extXfull]
    r.add(s1.line(extXfull, extYfull, color=c, alpha=0.8, legend_label=lab))
    if idx != 1:    
        for ri in r:
            ri.visible = False
    else:
#       s1.line([extX[0], extX[0]], [0, extY[0]], color='black', line_dash="dashed", alpha=0.5)
#       s1.line([extX[1], extX[1]], [0, extY[1]], color='black', line_dash="dashed", alpha=0.5)
#       s1.add_layout(Label(x=extX[0], y=extY[0], text='%d' % int(extY[0]), text_font_size="10pt"))
#       s1.add_layout(Label(x=extX[0], y=extY[1], text='%d' % int(extY[1]), text_font_size="10pt"))
#       s1.add_layout(Label(x=extX[0], y=0, text='%d/%02d' % (extX[0].day, extX[0].month), text_font_size="10pt"))
#       s1.add_layout(Label(x=extX[1], y=0, text='%d/%02d' % (extX[1].day, extX[1].month), text_font_size="10pt"))
        ymax = extY[1] * 2

s1.y_range = Range1d(0, ymax)
s1.legend.location = "top_left"
s1.legend.click_policy = "hide"
s1.legend.label_text_font_size = "8px"
s1.add_layout(s1.legend[0], 'right')
s1.title.text = 'Exponential and Hyperbolic Tangent Fit - Total Confirmed Cases'
s1.yaxis.axis_label = 'Total Confirmed Cases'
s1.xaxis.axis_label = 'Date'
s1.xaxis.formatter = DatetimeTickFormatter(days = ['%F'])
s1.xaxis.major_label_orientation = -np.pi/4
s2.y_range = Range1d(0, 15)
s2.legend.location = "top_left"
s2.legend.click_policy = "hide"
s2.legend.label_text_font_size = "8px"
s2.add_layout(s2.legend[0], 'right')
s2.title.text = 'Doubling time (Days) - Total Confirmed Cases'
s2.yaxis.axis_label = 'Doubling Time (days)'
s2.xaxis.axis_label = 'Date'
s2.xaxis.formatter = DatetimeTickFormatter(days = ['%F'])
s2.xaxis.major_label_orientation = -np.pi/4

# Write xls data to file
workbook_cases.save('output.xls')

#
# Now do the whole thing again for Deaths.  This should *really* be written as a function!
#

# Deaths workbook set-up

print("")
print("")
print("############################################")
print("#                                          #")
print("#             Deaths Analysis              #")
print("#                                          #")
print("############################################")
print("")
print("")


# Add a sheet for summary
sumSheet = workbook_deaths.add_sheet('Summary')
sumSheet.write(0, 0, "Country")
sumSheet.write(0, 1, "Incidence")
sumSheet.write(0, 2, "Cumulative")
sumSheet.write(0, 3, "Doubling time (days, 3-day fit)")
sumSheet.write(0, 4, "Doubling time (days, 5-day fit)")
# Add a sheet for incidence
incSheet = workbook_deaths.add_sheet('Incidence')
incSheet.write(0, 0, "Date")
for date in range(maxDate, minDate - 1, -1):
    incSheet.write(maxDate - date + 1, 0, date, date_xf)
# Add a sheet for prevalence
preSheet = workbook_deaths.add_sheet('Cumulative')
preSheet.write(0, 0, "Date")
for date in range(maxDate, minDate - 1, -1):
    preSheet.write(maxDate - date + 1, 0, date, date_xf)
# Add a sheet for 3-Day Exponent of Growth in Prevalence
co3Sheet = workbook_deaths.add_sheet('Cumulative Exponent (3 Day)')
co3Sheet.write(0, 0, "Date (retrospective fit)")
co3Sheet.write(0, 1, "Date (centred fit)")
for date in range(maxDate, minDate - 1, -1):
    co3Sheet.write(maxDate - date + 1, 0, date, date_xf)
    co3Sheet.write(maxDate - date + 2, 1, date, date_xf)
# Add a sheet for 3-Day Estimate of Doubing time
dbl3Sheet = workbook_deaths.add_sheet('Doubling time (3 Day)')
dbl3Sheet.write(0, 0, "Date (retrospective fit)")
dbl3Sheet.write(0, 1, "Date (centred fit)")
for date in range(maxDate, minDate - 1, -1):
    dbl3Sheet.write(maxDate - date + 1, 0, date, date_xf)
    dbl3Sheet.write(maxDate - date + 2, 1, date, date_xf)
# Add a sheet for 5-Day Exponent of Growth in Prevalence
co5Sheet = workbook_deaths.add_sheet('Cumulative Exponent (5 Day)')
co5Sheet.write(0, 0, "Date (retrospective fit)")
co5Sheet.write(0, 1, "Date (centred fit)")
for date in range(maxDate, minDate - 1, -1):
    co5Sheet.write(maxDate - date + 1, 0, date, date_xf)
    co5Sheet.write(maxDate - date + 3, 1, date, date_xf)
# Add a sheet for 5-Day Estimate of Doubing time
dbl5Sheet = workbook_deaths.add_sheet('Doubling time (5 Day)')
dbl5Sheet.write(0, 0, "Date (retrospective fit)")
dbl5Sheet.write(0, 1, "Date (centred fit)")
for date in range(maxDate, minDate - 1, -1):
    dbl5Sheet.write(maxDate - date + 1, 0, date, date_xf)
    dbl5Sheet.write(maxDate - date + 3, 1, date, date_xf)
idx = 0
# For each chosen country
for g, c in zip(euGeoids, itertools.cycle(cList[10])):
    idx = idx + 1
    # Find the relevant rows, and extract days and case counts
    rows = [i for i in range(len(geoid)) if geoid[i] == g]
    day = np.flip( np.array( [dates[i] for i in rows] ).astype(float) )
    inc = np.flip( np.array( [deaths[i] for i in rows] ).astype(float) )

    # Write summary data to sheet
    sumSheet.write(idx, 0, g)
    if len(inc) > 0:
        sumSheet.write(idx, 1, int(inc[-1]))
    sumSheet.write(idx, 2, int(np.sum(inc)))

    # Write incidence and prevalence values to their sheets
    incSheet.write(0, idx, g) # Header in row 0
    for d, val in zip(day, inc):
        incSheet.write(int(maxDate - d) + 1, idx, val)
    preSheet.write(0, idx, g) # Header in row 0
    for d, val in zip(day, np.cumsum(inc)):
        preSheet.write(int(maxDate - d) + 1, idx, val)

    # Remove days with no detections
    day = day[inc != 0]
    inc = inc[inc != 0]
    
    # Find prevalence
    pre = np.cumsum(inc)
    incDays = day           # day will be modified with pre
    
    # Spreadsheet of Exponents
    # Write the header in row 0
    co3Sheet.write(0, idx+1, g)
    dbl3Sheet.write(0, idx+1, g)
    co5Sheet.write(0, idx+1, g)
    dbl5Sheet.write(0, idx+1, g)
    if day.size < 3:
        print("Error: Insufficient prevalence " + g)
        continue
    # Prep some variables and loop from day[0] to last day
    day0_deaths = int(day[0])
    dbl = np.zeros(maxDate - day0_deaths + 1)
    ci = np.zeros((2, maxDate - day0_deaths + 1))
    for i in range(day0_deaths, maxDate + 1):
        # Find the index of the data closest to target date
        ii = (np.abs(day - i)).argmin()
        # Perform the local fit, store exponent and doubling time in sheets
        if ii > 1 and ii < len(day):
            co3 = np.polyfit(day[ii-2:ii+1], np.log(pre[ii-2:ii+1]), 1)[0]
            co3Sheet.write(int(maxDate - i) + 1, idx+1, round(co3, 2))
            dbl3Sheet.write(int(maxDate - i) + 1, idx+1, round(np.log(2) / co3, 2))
        if ii > 3 and ii < len(day):
            co5 = np.polyfit(day[ii-4:ii+1], np.log(pre[ii-4:ii+1]), 1)[0]
            f = sm.OLS(np.log(pre[ii-4:ii+1]), sm.add_constant(day[ii-4:ii+1])).fit()
            co5 = f.params[1]       # slope
            ci5 = f.conf_int(0.05)[1] # slope confidence 
            co5Sheet.write(int(maxDate - i) + 1, idx+1, round(co5, 2))
            dbl5Sheet.write(int(maxDate - i) + 1, idx+1, round(np.log(2) / co5, 2))
            dbl[i - day0_deaths] = np.log(2) / co5
            ci[:, i - day0_deaths] = np.log(2) / ci5
    sumSheet.write(idx, 3, round(np.log(2)/co3, 2))
    sumSheet.write(idx, 4, round(np.log(2)/co5, 2))

    # Only fit curve when prevalence exceeds 20
    age = day[-1] - day0_deaths
    day = day[pre > 10]
    if day.size < 3:
        print("Error: Insufficient prevalence " + g)
        continue
    pre = pre[pre > 10]
    dbl = dbl[int(day[0]) - day0_deaths:] # Cut off noisy doubling data (low prevalence)
    ci = ci[:, int(day[0]) - day0_deaths:]

    # Fit exponent only by normalising on last day
    preMag = pre[-1]
    pre = pre / preMag      # pre is now fractional, ending in 1
    dateShift = day[-1]
    day = day - dateShift   # day is now negative (integers), ending in 0
    
    # Linear fit of log data
    coef1 = np.polyfit(day, np.log(pre), 1)
    #coef1 = np.linalg.lstsq(day[:, None], np.log(pre[:, None]), rcond=None)
    #print(coef1)
   
    # Weighted linear fit of log data
    coef2 = np.polyfit(day, np.log(pre), 1, w=np.sqrt(pre))
    #print(coef2)

    # Fit exponential directly via arbitrary function fit
    #f = lambda t, a: np.exp(a * t) # Function to fit
    #f = lambda t, c, a: c * np.exp(a * t) # Function to fit
    f = lambda t, *p: p[1] * np.exp(p[0] * t)
    coef3 = curve_fit(f,  day,  pre, p0=[1, 0.25]) #, sigma=pre)
    coef3 = coef3[0]
    print(g, coef3[0], coef2[0], coef1[0])

    # Fit a tanh function.  Not interfering with previous work 
    # as fitting the exponential gives info on the doubling time
    f1 = lambda t, *tanh_p: tanh_p[0]*(1.0+np.tanh((t-tanh_p[2])/tanh_p[1]))
    try:
        tanh_coef = curve_fit(f1, day, pre, p0=[500.0, 30.0, 0.0])
    except:
        print("Failed to fit tanh for",g)
    else:
        tanh_coef = tanh_coef[0]
        print("Tanh fit:",tanh_coef[0], tanh_coef[1], tanh_coef[2])
    tanh0 = tanh_coef[0]
    tanh1 = tanh_coef[1]
    tanh2 = tanh_coef[2]

    # National age of epidemic inferred by exponential from 1 case
    scal = coef3[1]
    expn = coef3[0]
#    if expn > 0:
#        age = np.log(preMag) / expn # y = 1/preMag = exp(a*t) => -log(preMag) = a*t
#    else:
#        age = -day[0]
    
    # Label string for plotting 
    lab_deaths = g + " %.2f" % expn
    if expn <= 0:
        print(lab_deaths)

    # bokeh plot
    r = set()
    # Original data darkers
    x3 = incDays.astype(int)   # Recover excel dates
    x3 = [datetime.fromordinal(datetime(1900, 1, 1).toordinal() + xi - 2) for xi in x3] # Excel date to python datetime
    fArr3 = [s3.circle, s3.square, s3.triangle]
    r.add(fArr3[idx//10](x3, np.cumsum(inc), color=c, alpha=0.8, legend_label=lab_deaths))
    # Fit line
    x4 = (day + day0_deaths + age).astype(int)   # Recover excel dates
    x4 = [datetime.fromordinal(datetime(1900, 1, 1).toordinal() + xi - 2) for xi in x4] # Excel date to python datetime
    r.add(s3.line(x4, preMag * scal * np.exp(day * expn), color=c, alpha=0.8, legend_label=lab_deaths))
    # Add tanh fit (I think!)
    r.add(s3.line(x4, preMag*tanh0*(1.0+np.tanh((day-tanh2)/tanh1)), color=c, alpha=0.8, legend_label=lab_deaths))
    # Doubling line
    x5 = (np.arange(0, len(dbl)) + 1 - len(dbl) + day0_deaths + age).astype(int)   # Recover excel dates
    x5 = [datetime.fromordinal(datetime(1900, 1, 1).toordinal() + xi - 2) for xi in x5] # Excel date to python datetime
    dash = ['solid', 'dashed', 'dotted', 'dotdash']
    r.add(s4.line(x5, dbl, color=c, legend_label=g, line_dash=dash[idx//10], line_width=2))
    # Doubling markers
    fArr4 = [s4.circle, s4.square, s4.triangle]
    r.add(fArr4[idx//10](x5, dbl, color=c, legend_label=g))
    errX_deaths = []
    errY_deaths = []
    for x, c0, c1 in zip(x5, ci[0,:], ci[1,:]):
        errX_deaths.append((x, x))
        errY_deaths.append((c0, c1))
    r.add(s4.multi_line(errX_deaths, errY_deaths, color=c, legend_label=g))
    # Fit Irish extrapolated markers
    expn = co3
    extX_deaths = [datetime(2020, 3, 31), datetime(2020, 4, 20)]
    if idx == 1:
        sumSheet.write(0, 5, extX_deaths[0].strftime("%d/%m/%Y"))
        sumSheet.write(0, 6, extX_deaths[1].strftime("%d/%m/%Y"))
    extY = [preMag * scal * np.exp((dt.toordinal() - ord0 - day0_deaths - age) * expn) for dt in extX]
    sumSheet.write(idx, 5, int(extY[0]))
    sumSheet.write(idx, 6, int(extY[1]))
    r.add(fArr3[idx//10](extX, extY, color=c, fill_color="white", legend_label=lab_deaths))
    extXfull_deaths = [x3[-1] + timedelta(days=d) for d in range(21)]
    extYfull_deaths = [preMag * scal * np.exp((dt.toordinal() - ord0 - day0_deaths - age) * expn) for dt in extXfull]
    r.add(s3.line(extXfull_deaths, extYfull_deaths, color=c, line_dash="dashed", alpha=0.8, legend_label=lab_deaths))
    # Add tanh fit (I think!)
    extYfull_deaths = [preMag*tanh0*(1.0+np.tanh((dt.toordinal() - ord0 - day0_deaths - age-tanh2) / tanh1)) for dt in extXfull_deaths]
    r.add(s3.line(extXfull_deaths, extYfull_deaths, color=c, alpha=0.8, legend_label=lab_deaths))
    if idx != 1:    
        for ri in r:
            ri.visible = False
    else:
#       s3.line([extX[0], extX[0]], [0, extY[0]], color='black', line_dash="dashed", alpha=0.5)
#       s3.line([extX[1], extX[1]], [0, extY[1]], color='black', line_dash="dashed", alpha=0.5)
#       s3.add_layout(Label(x=extX[0], y=extY[0], text='%d' % int(extY[0]), text_font_size="10pt"))
#       s3.add_layout(Label(x=extX[0], y=extY[1], text='%d' % int(extY[1]), text_font_size="10pt"))
#       s3.add_layout(Label(x=extX[0], y=0, text='%d/%02d' % (extX[0].day, extX[0].month), text_font_size="10pt"))
#       s3.add_layout(Label(x=extX[1], y=0, text='%d/%02d' % (extX[1].day, extX[1].month), text_font_size="10pt"))
        ymax = extY[1] * 2


s3.y_range = Range1d(0, ymax)
s3.legend.location = "top_left"
s3.legend.click_policy = "hide"
s3.legend.label_text_font_size = "8px"
s3.add_layout(s3.legend[0], 'right')
s3.title.text = 'Exponential and Hyperbolic Tangent Fits - Total Deaths'
s3.yaxis.axis_label = 'Total Deaths'
s3.xaxis.axis_label = 'Date'
s3.xaxis.formatter = DatetimeTickFormatter(days = ['%F'])
s3.xaxis.major_label_orientation = -np.pi/4
s4.y_range = Range1d(0, 15)
s4.legend.location = "top_left"
s4.legend.click_policy = "hide"
s4.legend.label_text_font_size = "8px"
s4.add_layout(s4.legend[0], 'right')
s4.title.text = 'Doubling time (Days) - Total Deaths'
s4.yaxis.axis_label = 'Doubling Time (days)'
s4.xaxis.axis_label = 'Date'
s4.xaxis.formatter = DatetimeTickFormatter(days = ['%F'])
s4.xaxis.major_label_orientation = -np.pi/4

output_file("index.html", title="COVID-19 Growth Analysis")
bot_cases = row(s1, s2)
bot_deaths = row(s3, s4)
par0 = Paragraph(text="""Authors: Sean J. Delaney Ph.D., Prof. Turlough P. Downes (DCU)      email: covid19ie@outlook.com""", width=1200, height=20)
par1 = Paragraph(text="""Disclaimer 1: We do not guarantee the accuracy of this data, analysis or predictive estimates.""", width=1200, height=20)
par2 = Div(text="""Disclaimer 2: <b>Any implied "predictions" are "wrong"!</b> They're simplistic, and based on delayed data, ignoring recent interventions and effects. In fact one should consider the plots here more as a potential indicator of changes in public policy, public behaviour, testing regime, disease definition etc""", width=1200, height=40)
par3 = Paragraph(text="""Prediction of patient numbers (prevalence) of COVID-19 is important to understand the anticipated demands on health service resources. This page (and project) was created to assist in that effort. We note that the effect of social distancing measures is delayed by many days (symptom onset + testing time). After that delay, the doubling times below should increase - INCREASING DOUBLING TIME IS GOOD NEWS - and the growth predictions decrease accordingly. We also note that this model is designed for the early growth phase of the outbreak.""", width=1200, height=60)
par4 = Paragraph(text="""This page is generated with COVID-19 data from the ECDC. Click on the plot legend to toggle country data on/off. The first plot shows an exponential fit (weighted to favour recent data), with the growth rate (exponent of growth) shown in the legend, and a hyperbolic-tangent fit (which might be more appropriate for later times in the epidemic). The difference in the predicted number of cases for each of these fits should be seen as a rough guide to how inaccurate these predictions are.  The second plot shows the number of days (a changing estimate) expected for patient numbers to double, i.e. the estimated instantaneous doubling time, by fitting an exponential curve to the 5 days before the target day. Error bars indicate how good a fit the exponential is (large error bars suggest non-exponential growth).""", width=1200, height=100)
par5 = Div(text="""Download spreadsheet <a href="https://bitbucket.org/covid19ie/covid19ie.bitbucket.io/raw/HEAD/output.xls">here.</a>""", width=200, height=20)
copyright = Div(text="Software copyright 2020, Sean J. Delaney and Turlough P. Downes. BSD 3-Clause Licence, see README.txt enclosed herewith.", width=1200, height=20)
p = column(copyright, par0, par1, par2, par3, par4, bot_cases, bot_deaths)
show(p)

# Write xls data to file
workbook_deaths.save('output_deaths.xls')
