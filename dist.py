#!/usr/bin/env python3

# Authors: Sean J. Delaney Ph.D., Prof. Turlough P. Downes Ph.D. (DCU)
# Copyright: See README.txt, enclosed herewith
# email: seanogdelaney@gmail.com
# Python script to analyse ECDC Covid-19 Geographic Distribution Spreadsheet
# Arguments: None
# Output: 
# dist.html : A webpage with statistics and interactive plots

import numpy as np                  # for numerical analysis
from scipy import stats             # for kurtosis etc

import xlrd, xlwt                   # for xls file IO
import re                           # to find xls link in landing page 
import requests                     # for web access 
import os.path                      # for filesystem access

from datetime import datetime       # For readable dates
import itertools                    # For line colour cycling

# Various pieces of the bokeh interactive plotting package
from bokeh.plotting import figure as bkfig, output_file, show
from bokeh.layouts import row, column
from bokeh.models import DatetimeTickFormatter, Range1d, Div, Band, ColumnDataSource
from bokeh.palettes import Category20 as cList

# Fetch the ECDC page where the latest file is published
page = requests.get('https://www.ecdc.europa.eu/en/publications-data/download-todays-data-geographic-distribution-covid-19-cases-worldwide').text
# Find the url of the latest published file, and download it
url = re.findall('https://www.ecdc.europa.eu/sites/default/files/documents/COVID-19-geographic-disbtribution-worldwide.*xlsx', page)[0]
fname = os.path.basename(url)       # Extract file name
if not os.path.isfile(fname):       # If not present, download file
    r = requests.get(url, allow_redirects=True)
    open(fname,'wb').write(r.content)
if os.path.isfile(fname):           # If present, print identified file name
    print('Found', fname)
else:                               # otherwise, quit with message
    print("Couldn't find latest COVID-19-geographic-disbtribution-worldwide")
    quit()

# Open xls workbook, first sheet
wb = xlrd.open_workbook(fname)
sheet = wb.sheet_by_index(0)

# Extract 3 columns: dates, cases, and geoid (country code)
dates = sheet.col_values(0) # stored as days since 30/12/1899 apparently
cases = sheet.col_values(4)
geoid = sheet.col_values(7)

# Currently unused list of chosen countries, shortened below:
country = ['Austria', 'Belgium', 'Bulgaria', 'Croatia', 'Cyprus', 'Czech Republic', 'Denmark', 'Estonia', 'Finland', 'France', 'Germany', 'Greece', 'Hungary', 'Republic of Ireland', 'Italy', 'Latvia', 'Lithuania', 'Luxembourg', 'Malta', 'Netherlands', 'Poland', 'Portugal', 'Romania', 'Slovakia', 'Slovenia', 'Spain', 'Sweden', 'United Kindom of Great Britain and Northern Ireland']

# Shortened country identifiers
euGeoids = ['DE', 'AT', 'BE', 'BG', 'HR', 'CY', 'CZ', 'DK', 'EE', 'FI', 'FR', 'IE', 'EL', 'HU', 'IT', 'LV', 'LT', 'LU', 'MT', 'NL', 'PL', 'PT', 'RO', 'SK', 'SL', 'ES', 'SE', 'UK']

# A shorter list for less cluttered plots
#euGeoids = ['DE', 'ES', 'FR', 'IE', 'IT', 'UK']

# Some initialisation for plots
f0 = bkfig(width=600, height=700)
f1 = bkfig(width=600, height=700)
f2 = bkfig(width=600, height=700, x_axis_type='datetime')
f3 = bkfig(width=600, height=700, x_axis_type='datetime')

# For each chosen country
for g, c in zip(euGeoids, itertools.cycle(cList[20])):

    # Find the relevant rows, and extract days and case counts
    rows = [i for i in range(len(geoid)) if geoid[i] == g]
    day = np.flip( np.array( [dates[i] for i in rows] ).astype(float) )
    inc = np.flip( np.array( [cases[i] for i in rows] ).astype(float) )
    if not np.sum(inc > 9):
        print('No useful data for', g)
        continue

    # Fix oddity in italian figures on 15/03 (tests logged next day?)
    if g == 'IT':
        oddDay = np.where(day == 43905)[0][0]
        inc[oddDay:oddDay+2] = np.mean(inc[oddDay:oddDay+2])

    # Find the index of the last increment under 10, and cull to there
    cutL = np.where(inc < 10)[0][-1]
    if cutL > len(inc) - 5: # Requirement for 1 data point
        print('No useful data for', g)
        continue
    inc = inc[cutL + 1:]
    day = day[cutL + 1:]
    
    # Find the 'exponents' for 1-Day and 2-Day Ratio
    d = np.vstack((inc[2:] / inc[1:-1], np.sqrt(inc[2:] / inc[:-2])))
    d /= np.mean(d, 1)[:, None]

    # Concatenate individual country data
    if g == 'DE':
        D = d
    else:
        D = np.hstack((D, d))

    # Convert excel dates to python datetimes
    x = [datetime.fromordinal(datetime(1899, 12, 30).toordinal() + int(d)) for d in day[1:-1]]

    # Track the extrema, for plot axis ranges (required for bokeh band shading)
    if g == 'DE':
        minX = min(x)
        maxX = max(x)
    else:
        minX = min([min(x), minX])
        maxX = max([max(x), maxX])

    # Per-country plots of normalised ratio
    f2.line(x, d[0,:], alpha=0.8, legend_label=g, color=c).visible = g == 'DE'
    f2.circle(x, d[0,:], alpha=0.8, legend_label=g, color=c).visible = g == 'DE'
    f3.line(x, d[1,:], alpha=0.8, legend_label=g, color=c).visible = g == 'DE'
    f3.circle(x, d[1,:], alpha=0.8, legend_label=g, color=c).visible = g == 'DE'

# Pooled data histogram
y1, x1 = np.histogram(D[0,:], bins=30)
y2, x2 = np.histogram(D[1,:], bins=30)
per1 = np.percentile(D[0,:], [0, 10, 40, 60, 90, 100])
per2 = np.percentile(D[1,:], [0, 10, 40, 60, 90, 100])

# Statistics
s1 = stats.describe(D[0,:])
s2 = stats.describe(D[1,:])

# Lower percentile shaded band
f2.x_range=Range1d(minX, maxX)
f2.y_range=Range1d(min(D[0,:]), max(D[0,:]))
source = ColumnDataSource({
        'base':[minX, maxX],
        'lower':[per1[0]]*2,
        'upper':[per1[1]]*2
        })
f2.add_layout(Band(base='base', lower='lower', upper='upper', source=source, fill_alpha=0.5))

# Middle percentile shaded band
source = ColumnDataSource({
        'base':[minX, maxX],
        'lower':[per1[2]]*2,
        'upper':[per1[3]]*2
        })
f2.add_layout(Band(base='base', lower='lower', upper='upper', source=source, fill_color='palegreen', fill_alpha=0.5))

# Upper percentile shaded band
source = ColumnDataSource({
        'base':[minX, maxX],
        'lower':[per1[4]]*2,
        'upper':[per1[5]]*2
        })
f2.add_layout(Band(base='base', lower='lower', upper='upper', source=source, fill_alpha=0.5))

# Lower percentile shaded band
f3.x_range=Range1d(minX, maxX)
f3.y_range=Range1d(min(D[0,:]), max(D[0,:]))
source = ColumnDataSource({
        'base':[minX, maxX],
        'lower':[per2[0]]*2,
        'upper':[per2[1]]*2
        })
f3.add_layout(Band(base='base', lower='lower', upper='upper', source=source, fill_alpha=0.5))

# Middle percentile shaded band
source = ColumnDataSource({
        'base':[minX, maxX],
        'lower':[per2[2]]*2,
        'upper':[per2[3]]*2
        })
f3.add_layout(Band(base='base', lower='lower', upper='upper', source=source, fill_color='palegreen', fill_alpha=0.5))

# Upper percentile shaded band
source = ColumnDataSource({
        'base':[minX, maxX],
        'lower':[per2[4]]*2,
        'upper':[per2[5]]*2
        })
f3.add_layout(Band(base='base', lower='lower', upper='upper', source=source, fill_alpha=0.5))

# Table of statistics
table = Div(text=" <table border='1'> <tr><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th></tr> <tr><td>%s</td><td>%d</td><td>%.2f</td><td>%.2f</td><td>%.2f</td><td>%.2f</td><td>%.2f</td><td>%.2f</td></tr> <tr><td>%s</td><td>%d</td><td>%.2f</td><td>%.2f</td><td>%.2f</td><td>%.2f</td><td>%.2f</td><td>%.2f</td></tr> </table> " % ('#', 'Count', 'Minimum', 'Maximum', 'Mean', 'Variance', 'Skew', 'Kurtosis', '1-day Ratio', s1[0], s1[1][0], s1[1][1], s1[2], s1[3], s1[4], s1[5], '2-Day Ratio', s2[0], s2[1][0], s2[1][1], s2[2], s2[3], s2[4], s2[5] ), width=1200, height=100)

# 1-day Ratio histogram
xMin = min(np.hstack((x1,x2)))
xMax = max(np.hstack((x1, x2)))
f0.quad(top=y1, bottom=0, left=x1[:-1], right=x1[1:])
f0.x_range=Range1d(xMin, xMax)
f0.title.text = 'Normalised 1-day Ratio Histogram'
f0.xaxis.axis_label = '1-Day Ratio'

# 2-Day Ratio Histogram
f1.quad(top=y2, bottom=0, left=x2[:-1], right=x2[1:])
f1.x_range=Range1d(xMin, xMax)
f1.title.text = 'Normalised 2-Day Ratio Histogram'
f1.xaxis.axis_label = '2-Day Ratio'

# 1-day Ratio ratio plot
f2.title.text = 'Normalised 1-day Ratio, DE'
f2.yaxis.axis_label = '1-Day Ratio'
f2.xaxis.axis_label = 'Date'
f2.xaxis.formatter = DatetimeTickFormatter(days = ['%F'])
f2.xaxis.major_label_orientation = -np.pi/4
f2.legend.click_policy = 'hide'
#f2.output_backend = 'svg' # Vector graphics. Makes page slow

# ratio plot
f3.title.text = 'Normalised 2-Day Ratio, DE'
f3.yaxis.axis_label = '2-Day Ratio'
f3.xaxis.axis_label = 'Date'
f3.xaxis.formatter = DatetimeTickFormatter(days = ['%F'])
f3.xaxis.major_label_orientation = -np.pi/4
f3.legend.click_policy = 'hide'

# Plot output
output_file('hist.html', title='COVID-19 Incidence Ratio Analysis')
copyright = Div(text="Software copyright 2020, Sean J. Delaney and Turlough P. Downes. BSD 3-Clause Licence, see README.txt enclosed herewith.", width=1200, height=20)
show(column(copyright, table, row(f0, f1), row(f2, f3)))
